<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/teacher','LeaveController@fun_indexteacher');

Route::get('/principal','LeaveController@fun_indexprincipal');


Route::get('/xx', function () {
    return view('xx');
});

Route::get('/register', function () {
    return view('register');
});
 
Route::post('/store','LeaveController@store');

Route::get('/login', function () {
    return view('login');
});

Route::post('/logs','LeaveController@logs');

Route::get('/leave', function () {
    return view('leave');
});

Route::get('/teacherw', function () {
    return view('teacherw');
});

Route::get('/teacherdetails',"TeacherController@index");
Route::get('/edit/{id}',"TeacherController@edit");
Route::get('/show/{id}',"TeacherController@show");
Route::get('/create',"TeacherController@create");
Route::post('/store',"TeacherController@store");
Route::post('/update/{id}',"TeacherController@update");
