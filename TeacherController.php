<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teacher;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     $teachers=Teacher::all();
    //     return view('teacher',['teachers'=>$teachers,'layout'=>'index']);
    // }
    public function index()
    {
        $teachers=Teacher::all();
        return view('teacherdetails',['teachers'=>$teachers,'layout'=>'index']);
    }

// public function index()
//     {
//          $contacts = "test"; //Teacher::all();
//         // return view('teacherdetails.blade', compact('contacts'));
//          dd($contacts);
//     }

//     public function index(){
// $teachers = DB::select('select * from teachers');
// return view('stud_view',['teachers'=>$teachers]);
// }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create()
    {
        $teachers=Teacher::all();
        return view('teacher',['teachers'=>$teachers,'layout'=>'create']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $teacher = new Teacher();
        $teacher->tno = $request->input('tno');
        $teacher->firstName = $request->input('firstName');
        $teacher->lastName = $request->input('lastName');
        $teacher->appoinmentDate = $request->input('appoinmentDate');
        $teacher->designation = $request->input('designation');
        $teacher->speciality = $request->input('speciality');
        $teacher->save();
        return redirect('/teacherw');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher = Teacher::find($id);
        $teachers=Teacher::all();
        return view('teacher',['teachers'=>$teachers,'teacher'=>$teacher,'layout'=>'show']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teacher::find($id);
        $teachers=Teacher::all();
        return view('teacher',['teachers'=>$teachers,'teacher'=>$teacher,'layout'=>'edit']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $teacher = Teacher::find($id);
        $teacher->tno = $request->input('tno');
        $teacher->firstName = $request->input('firstName');
        $teacher->lastName = $request->input('lastName');
        $teacher->appoinmentDate = $request->input('appoinmentDate');
        $teacher->designation = $request->input('designation');
        $teacher->speciality = $request->input('speciality');
        $teacher->save();
        return redirect('/teacherw');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacher = Teacher::find($id);
        $teacher->delete();
        return redirect('/teacherw');
    }
}
